
import tkinter, os, pygame
from tkinter import messagebox, Button, Canvas, Entry, Label, Text
top = tkinter.Tk()
top.title('Tkinter Test GUI')
top.geometry("600x600")



#Create a listbox with a scroll bar within a frame.
def get_audio():
    user_directory = entryfield.get() # Note: Scope of 'user_directory' variable within this function, is different to the 'user_directory' variable from the 'get_directory()' function.
    filename=listbox.get('active')
    print("===============")
    print("Filename: " + filename)
    print("Absolute Path: " + user_directory + filename)
    pygame.init()
    pygame.mixer.music.load(user_directory + filename) # Accessing files outside of the python directory requires the absolute (full) path, hence => path + filename. 
    pygame.mixer.music.play()

def get_directory(): # Will add files within the user specified directory path to the listbox. Must use and end with '/' => eg. C:/Users/Luke Di Mauro/Desktop/audiotest/
    user_directory = entryfield.get()
    print(user_directory)
    dirs_2 = os.listdir( user_directory )
    for file_2 in dirs_2:
     print(file_2)
     listbox.insert('end', file_2)



#Create elements
frame = tkinter.Frame(top, bd=10, relief='sunken', width=150, height=300)
scrollbar = tkinter.Scrollbar(frame)
listbox = tkinter.Listbox(frame)


# Create user directory input.
label_1 = Label(top, text="Please Input Directory - Must use and end with '/' (eg. C:/Users/documents/)")
label_1.pack( side='top')
entryfield = Entry(top, bd=5, width=50)
entryfield.pack(side='top')
pathbutton=Button(top, text="Confirm Audio Path", command=get_directory)
pathbutton.pack(side='top')


#Attach listbox to scrollbar
listbox.config(yscrollcommand=scrollbar.set)
scrollbar.config(command=listbox.yview)


#Pack elements
frame.pack(side='top')
scrollbar.pack(side='right', fill='y')
listbox.pack()


# Create button to confirm highlighted scrollbar selection.
selectbutton=Button(top, text="Select Audio File", command=get_audio)
selectbutton.pack(side='top')

top.mainloop()
